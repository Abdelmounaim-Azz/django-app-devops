variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-devops-learning"
}

variable "contact" {
  default = "abdelmounaim.azz@outlook.fr"
}

variable "db_username" {
  description = "username for postgres instance"
}

variable "db_password" {
  description = "password for postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-bastion-key"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "393306807130.dkr.ecr.us-east-1.amazonaws.com/recipeapp-app-recipe-devops"
}

variable "ecr_image_proxy" {
  description = "ECR Image for Proxy"
  default     = "393306807130.dkr.ecr.us-east-1.amazonaws.com/recipeapp-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
